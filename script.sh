#!/usr/bin/env bash

set -e

if [ "x$1" == "x" ]; then
    echo work dir is .
else
    echo work dir is $1
    cd $1
fi

#echo use config
#. ./config

echo adding non-free to apt list
cp sources.list /etc/apt
apt-get update

echo install stuff
apt-get update
apt-get install -y hostapd dnsmasq
apt-get install -y usbutils

apt-get install -y firmware-ath9k-htc firmware-atheros

echo set up interfaces, ips and iptables

# yes, I am using old-style interfaces file
cp interfaces /etc/network/
systemctl disable systemd-networkd
#ifdown $WLAN_INT && ifup $WLAN_INT 
cp sysctl.conf /etc
cp iptables.ipv4.nat /etc
iptables-restore < /etc/iptables.ipv4.nat

echo set up dnsmasq
cp dnsmasq.conf /etc
systemctl disable systemd-resolved
#systemctl stop systemd-resolved
#service dnsmasq restart

echo set up hostapd
cp hostapd.conf /etc/hostapd
cp hostapd /etc/default
systemctl unmask hostapd
systemctl enable hostapd
#service hostapd restart

echo Everything should now be installed.
echo a reboot might be needed
