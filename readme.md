hostapd
=============

**NB**: It is not working with the usb->wireless devices I have. I'm simply not getting any trafic on the wlan interface.
Either it is a config thing or a hardware thing, so testing it directly on a raspberry with wireless would be nice.

This is a simple test setup using a wireless device and set up an access point.

Most of this is based on the guide for [raspberry AP](https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md)

we have some services that need to work

* `dnsmasq` gives DHCP stuff
* `hostapd` the actual access point software
* `iptables`and forwarding.
